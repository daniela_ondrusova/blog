<?php 

    include 'database_connectie.php';
    include 'header.php';

    $id = $_GET['id'];
    $stmt1 = $pdo->query("SELECT * FROM posts WHERE id = $id");
    $row = $stmt1->fetch(); 
?>

<div class="w-full max-w-xl m-auto pt-12 pb-6 flex-grow">
    <h1 class="text-4xl font-bold px-8"><?php echo $row['title']; ?></h1>
    <form method="POST" class="bg-white px-8 pt-6 pb-8 mb-4">
        <div class="mb-4">
            <label for="title" class="block text-gray-700 text-sm font-bold mb-2">
                <span>Title</span>
                <input type="text" id="title" name="title" value="<?php echo $row['title']; ?>" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
            </label>
        </div>
        <div class="mb-4">
            <label for="content" class="block text-gray-700 text-sm font-bold mb-2">
                <span>Content</span>
                <textarea id="content" name="content" rows="10" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"><?php echo $row['content']; ?></textarea>
            </label>
        </div>
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Edit</button>
            
    </form>
</div>

<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST['title'] != $row['title'] || $_POST['content'] != $row['content']) {
        foreach ($_POST as $column => $value) {
            $stmt1 = $pdo->prepare("UPDATE posts SET $column = '$value' WHERE id = $id");
            $stmt1->execute();
        }
        header("Location: admin.php");
        exit(); 
    } 
}
include 'footer.php';
?>
