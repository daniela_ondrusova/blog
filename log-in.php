<?php

    include 'database_connectie.php';
    include 'header.php';
    if (isset($_SESSION['user'])) {
        header("location: admin.php");
        die;
    }  

    $message = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST['user_name'];
        $password = $_POST['user_password'];
        
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_name = ?");
        $stmt->bindParam(1, $username);
        $stmt->execute();
    
        $user = $stmt->fetch();
    
        if ($user && $user['user_password']) {
            $_SESSION['user'] = $user['type_user'];
            header('Location: admin.php'); 
            exit();
        } else {
            $message = "Invalid name or password.";
        }
    }
    
?>
    <div class="pt-12 border-b flex-grow">
        <div class="w-full max-w-xs m-auto">
            <form class="bg-white px-8 py-6" method="POST">
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Username
                    </label>
                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" name="user_name" placeholder="Username" required>
                </div>
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                        Password
                    </label>
                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" name="user_password" placeholder="******************" required>
                </div>
                <p class="text-red-500 mb-5"><?php echo $message ?></p>
                <div class="flex items-center justify-between">
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                        Sign In
                    </button>
                </div>
            </form>
        </div>
    </div>
<?php include 'footer.php'?>        
