<?php 

include 'database_connectie.php';
include 'header.php';
if ($_SESSION['user'] != "a") {
    header ("location: admin.php");
}
?>

<div class="pt-12 pb-6 flex-grow">
    <table class="table-auto w-full mt-8">
        <thead>
            <tr>
            <th class="px-4 py-2 text-xl">User name</th>
            <th class="px-4 py-2 text-xl">Email</th>
            <th class="px-4 py-2 text-xl">Type user</th>
            </tr>
        </thead>
        <tbody>
        <?php

            $stmt = $pdo->query('SELECT * FROM users'); 
            while ($row = $stmt->fetch()) { 
                $id = $row['id'];
                ?>
            <tr>
            <td class="px-4 py-2 text-center"><?php echo $row['user_name']?></td>
            <td class="px-4 py-2 text-center"><?php echo $row['email']?></td>
            <td class="px-4 py-2 text-center"><?php echo $row['type_user']?></td>
            
            </tr>             
        <?php } ?>

        </tbody>
    </table>
</div>


<?php include 'footer.php'?>

    
