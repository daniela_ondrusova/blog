CREATE DATABASE IF NOT EXISTS blog;
USE blog;

CREATE TABLE IF NOT EXISTS posts (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO posts (title, content)
VALUES 
("Cupcake", "<p>Cupcake ipsum dolor sit amet pie. Pastry gummi bears marzipan marzipan apple pie dessert sweet roll. Chocolate tiramisu jujubes jujubes topping toffee sesame snaps.</p>

<p>Jelly marzipan carrot cake icing cookie shortbread soufflé. Macaroon pie sugar plum lemon drops marzipan gingerbread cake cheesecake croissant. Sweet jelly-o lollipop gingerbread icing ice cream gingerbread. Cake chocolate bar cheesecake gummi bears danish cookie cake.</p>

<h3>Liquorice toffee</h3>
<p>Liquorice toffee dragée shortbread carrot cake. Lollipop jelly wafer sesame snaps cheesecake jelly-o jelly beans muffin. Muffin chocolate bar gummi bears gingerbread sesame snaps. Tart fruitcake chocolate muffin caramels.</p>

<p>Tootsie roll marshmallow halvah gingerbread candy. Bonbon biscuit sweet roll sweet roll jelly fruitcake. Lollipop carrot cake carrot cake donut fruitcake candy jelly beans pastry. Donut wafer oat cake cheesecake biscuit.</p>

<p>Pudding lemon drops muffin dragée cheesecake marzipan pie. Dessert biscuit sugar plum chupa chups bear claw chupa chups. Biscuit bonbon croissant candy canes pie candy canes.</p>"),

("Jelly marzipan", "<p>Chocolate tiramisu jujubes jujubes topping toffee sesame snaps.</p>

<h3>Dessert biscuit</h3>
<p>Dessert biscuit sugar plum chupa chups bear claw chupa chups. Biscuit bonbon croissant candy canes pie candy canes.</p>

<p>Macaroon pie sugar plum lemon drops marzipan gingerbread cake cheesecake croissant. Sweet jelly-o lollipop gingerbread icing ice cream gingerbread. Cake chocolate bar cheesecake gummi bears danish cookie cake.</p>

<p>Muffin chocolate bar gummi bears gingerbread sesame snaps. Tart fruitcake chocolate muffin caramels.</p>

<h3>Lollipop carrot cake</h3>
<p>Lollipop carrot cake carrot cake donut fruitcake candy jelly beans pastry. Donut wafer oat cake cheesecake biscuit.</p>

<p>Dessert biscuit sugar plum chupa chups bear claw chupa chups. Biscuit bonbon croissant candy canes pie candy canes.</p>");


CREATE TABLE IF NOT EXISTS users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_name VARCHAR(255) NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    type_user VARCHAR(255) NOT NULL
);

INSERT INTO users (user_name, user_password, email, type_user)
VALUES 
("editor", password("edit0r"), "editor@gmail.com", "e"),
("admin", password("@dmin"), "admin@gmail.com", "a");