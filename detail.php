
<?php 
    
    include 'database_connectie.php';
    include 'header.php';
    $id = $_GET['id']; 
    $stmt = $pdo->query("SELECT * FROM posts WHERE id = $id");
    $row = $stmt->fetch();
?>
    <div class="flex-grow">
        <div class="space-y-5 text-center pt-12 pb-6 border-b">
            <dl>
                <dt class="sr-only">Published on</dt>
                <dd class="text-base font-medium leading-6 text-gray-500">
                    <time><?php echo $row['created_at'] ?></time>
                </dd>
            </dl>
            <h1 class="text-4xl font-bold"><?php echo $row['title']?></h1>
        </div>
        <div class="pt-10 my_content">
            <p><?php echo $row['content'] ?></p>
        </div>

    </div>
<?php include 'footer.php'?>
