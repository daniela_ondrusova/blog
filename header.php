<?php

session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body class="h-screen">
    <div class="bg-white text-black w-8/12 my-0 mx-auto pt-10 flex flex-col h-screen">
        <nav class="flex justify-between bg-white font-semibold py-6 items-end sticky top-0">
            <div class="flex items-end flex-shrink-0 text-gray-900">
                <img src="./interface-control-svgrepo-com.svg" alt="img" class="w-8 mr-4">
                <a href="index.php" class="text-2xl tracking-tight">Mijn Blog</a>
            </div>
            <div class="w-full block flex-grow flex w-auto">
                <div class="text-m flex flex-grow flex justify-end">
                <a href="index.php" class="block mt-4 inline-block mt-0 text-gray-900 hover:text-gray-500 mr-6">
                    Home
                </a>             
                    <?php                      
                    
                    if (isset($_SESSION['user'])) {                        
                        echo "<a href='admin.php' class='block mt-4 inline-block mt-0 text-gray-900 hover:text-gray-500 mr-6'>Admin</a>
                        <a href='log-out.php' class='block mt-4 inline-block mt-0 text-gray-900 hover:text-gray-500 mr-6'>Log out</a>";
                    } else {
                        echo "
                        <a href='log-in.php' class='block mt-4 inline-block mt-0 text-gray-900 hover:text-gray-500 mr-6'>Log in</a>";
                    }                  

                 ?>
                
                </div>
            </div>
        </nav>
