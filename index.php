<?php include 'header.php'?>

    <div class="pt-12 pb-6 border-b flex-grow">
        <h1 class="text-4xl font-bold">Recent articles</h1>
        <p class="text-gray-500 mt-5">A blog created with PHP and Tailwind</p>
    </div>
    <div>
        <ul class="mb-10">
<?php

    include 'database_connectie.php';
    while ($row = $stmt->fetch()) {
?>
            <li class="py-12 border-b">
                <div class="space-y-2 xl:grid xl:grid-cols-4 xl:items-baseline xl:space-y-0">
                    <dl>
                        <dt class="sr-only">Published on</dt>
                        <dd class="text-base font-medium leading-6 text-gray-500">
                            <time><?php echo $row['created_at'] ?></time>
                        </dd>
                    </dl>
                        
                    <div class="space-y-5 xl:col-span-3">
                        <h2 class="text-2xl font-bold leading-8 tracking-tight">
                            <a class="text-gray-900" href=""><?php echo $row['title'] ?></a>
                        </h2>
                                                
                        <div class="prose max-w-none text-gray-500 my_content"><?php echo substr($row['content'], 0, 150).'...' ?>
                        </div>
                                        
                        <div class="text-base font-medium leading-6">
                            <a class="text-blue-500 hover:text-blue-700" href="./detail.php?id=<?php echo $row['id']; ?>">Read more →</a>
                        </div>
                    </div>
                </div>
            </li>
<?php } ?>
        </ul>
    </div>
<?php include 'footer.php'?>        
