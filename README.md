## Functionaliteiten van mijn blog

### Gebruikers kunnen

- Een overzicht van alle artikelen zien op de homepagina.
- Het volledige artikel bekijken op de detailpagina.
- Naar de loginpagina gaan.

### Inlogsectie

- De formulierinvoer wordt gevalideerd om te controleren of de gebruiker een administrator of redacteur is.

### Redacteurs kunnen

- Een overzicht van alle artikelen zien.
- Artikelen toevoegen en bewerken.
- Artikelen verwijderen.

### Beheerders kunnen

- Alles doen wat een redacteur kan.
- Alle gebruikers bekijken.

Er is gebruik gemaakt van een Tailwind-framework.

## Hoe een PHP-server te starten met XAMPP

### 1. Installeer XAMPP

- Download en installeer XAMPP vanaf [de XAMPP-website](https://www.apachefriends.org/index.html).

### 2. Start XAMPP

- Open het XAMPP-controlepaneel via het startmenu of een vergelijkbare methode op jouw besturingssysteem.

### 3. Start Apache

- Zoek in het XAMPP-controlepaneel naar "Apache" en klik op de knop "Start" om de Apache-webserver te starten.

### 4. Plaats bestanden in de "htdocs"-map

- Plaats je PHP-bestanden in de "htdocs"-map van XAMPP. Bijvoorbeeld: `C:\xampp\htdocs` op Windows.

### 5. Toegang tot je PHP-toepassing

- Open je webbrowser en ga naar `http://localhost/jouw_bestandsnaam.php`, waarbij je "jouw_bestandsnaam.php" vervangt door de daadwerkelijke naam van je PHP-bestand.  