<?php 
include 'database_connectie.php';
include 'header.php'?>

<div class="w-full max-w-xl m-auto flex-grow">
    <form method="POST" class="bg-white px-8 pt-6 pb-8 mb-4">
        <div class="mb-4">
            <label for="title" class="block text-gray-700 text-sm font-bold mb-2">
                <span>Title</span>
                <input type="text" id="title" name="title" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
            </label>
        </div>
        <div class="mb-4">
            <label for="content" class="block text-gray-700 text-sm font-bold mb-2">
                <span>Content</span>
                <textarea id="content" name="content" rows="10" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"></textarea>
            </label>
        </div>
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Add</button>
            
    </form>
</div>

<?php 

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $data = [
            'title' => $_POST['title'],
            'content' => $_POST['content']
        ];

        $sql = "INSERT INTO posts (title, content) 
                VALUES (:title, :content)";

        $stmt = $pdo->prepare($sql);
        $stmt->execute($data);

        header("Location: admin.php");
        exit();
    }
    
    include 'footer.php'
?>
