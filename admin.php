<?php 

    include 'database_connectie.php';
    include 'header.php';
    if (!isset($_SESSION['user'])) {
        header("location: log-in.php");
        die;
    }  

?>

    <div class="flex-grow">
        <div class="pt-12 pb-6">
            <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3" role="alert">
                <p class="font-bold">
                    <?php
                        if ($_SESSION['user'] == "a") {                        
                            echo "You are logged in as an administrator.";
                        } else {
                            echo "You are logged in as an editor.";
                        }
                    ?>
                </p>
            </div>
        </div>
        <div class="pt-8 pb-6">
            <div class="flex justify-between">
                <a href="add.php" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Add new article</a>

                <?php
                
                if ($_SESSION['user'] == "a") { ?>
                    <a href="users.php" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Users</a>
                <?php 

                }
                ?>
                
            </div>

            <table class="table-auto w-full mt-8">
                <thead>
                    <tr>
                    <th class="px-4 py-2 text-xl">Date</th>
                    <th class="px-4 py-2 text-xl">Title</th>
                    <th class="px-4 py-2 text-xl">Content</th>
                    <th class="px-4 py-2 text-xl">Edit</th>
                    <th class="px-4 py-2 text-xl">Delete</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    while ($row = $stmt->fetch()) { 
                        $id = $row['id'];
                        ?>
                    <tr>
                    <td class="px-4 py-2 text-center"><?php echo $row['created_at']?></td>
                    <td class="px-4 py-2 text-center"><?php echo $row['title']?></td>
                    <td class="px-4 py-2 text-center"><?php echo substr($row['content'], 0, 20).'...' ?></td>
                    <td class="px-4 py-2 text-center"><a href="edit.php?id=<?php echo $id ?>" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">edit</a></td>
                    <td class="px-4 py-2 text-center"><a href="delete.php?id=<?php echo $id ?>" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">delete</a></td>
                    </tr>             
                <?php } ?>

                </tbody>
            </table>
        </div> 
    </div>      
<?php include 'footer.php'?>
